package ro.home.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	//http://localhost:8080/product/1
	@RequestMapping("product/{id}")
	public String getProduct(@PathVariable("id") int id)
	{
		return "Product" + id;
	}
	
	//http://localhost:8080/productIds?id=2
	@RequestMapping("productIds")
	public List<Integer> getProductIds(@RequestParam("id") int id)
	{
		return Arrays.asList(id +1, id+2, id+3);
	}
}
